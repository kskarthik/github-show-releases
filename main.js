const repo = [document.baseURI.split("/")[3], document.baseURI.split("/")[4]];

if (
  document.querySelector(
    '[aria-label="Releases and Tags navigation buttons"]'
  ) &&
  !document.querySelector("#addon-release")
) {
  const a = document.createElement("a");
  a.href = `/${repo[0]}/${repo[1]}/releases.atom`;
  a.id = "addon-release";
  a.setAttribute("class", "js-selected-navigation-item subnav-item");
  a.textContent = "Atom Feeds 🔔";

  document
    .querySelector('[aria-label="Releases and Tags navigation buttons"]')
    .appendChild(a);
} else {
  console.log("element already exists");
}
